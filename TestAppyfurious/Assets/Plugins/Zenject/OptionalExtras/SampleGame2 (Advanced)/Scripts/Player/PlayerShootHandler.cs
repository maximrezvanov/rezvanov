using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;



namespace Zenject.SpaceFighter
{
    public class PlayerShootHandler : ITickable
    {
        readonly AudioPlayer _audioPlayer;
        readonly Player _player;
        readonly Settings _settings;
        readonly Bullet.Factory _bulletFactory;
        readonly PlayerInputState _inputState;
        private float timeShot;
        readonly float startTime = 0.5f;







        float _lastFireTime;

        public PlayerShootHandler(
            PlayerInputState inputState,
            Bullet.Factory bulletFactory,
            Settings settings,
            Player player,
            AudioPlayer audioPlayer)
        {
            _audioPlayer = audioPlayer;
            _player = player;
            _settings = settings;
            _bulletFactory = bulletFactory;
            _inputState = inputState;
        }

        public void Tick()
        {
            if (_player.IsDead)
            {
                return;
            }
            if (timeShot <= 0)
            {


                if (_inputState.IsFiring && Time.realtimeSinceStartup - _lastFireTime > _settings.MaxShootInterval)
                {
                    timeShot = startTime;
                    _lastFireTime = Time.realtimeSinceStartup;
                    Fire();
                }
            }
            else
            {
                timeShot -= Time.deltaTime;
            }
        }

        void Fire()
        {
            _audioPlayer.Play(_settings.Laser, _settings.LaserVolume);

            var bulletLeft = _bulletFactory.Create(
                _settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromPlayer);
            bulletLeft.transform.position = _player.Position + _player.LookDir * _settings.BulletOffsetDistance;
            bulletLeft.transform.rotation = _player.Rotation * Quaternion.Euler(0f, 0f, -_settings.tiltAngle);

            var bulletCenter = _bulletFactory.Create(
                _settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromPlayer);
            bulletCenter.transform.position = _player.Position + _player.LookDir * _settings.BulletOffsetDistance;
            bulletCenter.transform.rotation = _player.Rotation;


            var bulletRight = _bulletFactory.Create(
                _settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromPlayer);
            bulletRight.transform.position = _player.Position + _player.LookDir * _settings.BulletOffsetDistance;
            bulletRight.transform.rotation = _player.Rotation * Quaternion.Euler(0f, 0f, _settings.tiltAngle);

        }

       


        [Serializable]
        public class Settings
        {
            public AudioClip Laser;
            public float LaserVolume = 1.0f;

            public float BulletLifetime;
            public float BulletSpeed;
            public float MaxShootInterval;
            public float BulletOffsetDistance;
            public float tiltAngle = 30f;
        }
    }
}
