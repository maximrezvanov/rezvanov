﻿using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class PlayerFacade : MonoBehaviour
    {
        Player _model;
        PlayerDamageHandler _hitHandler;

        public GameObject gameOverWindow;

        [Inject]
        public void Construct(Player player, PlayerDamageHandler hitHandler)
        {
            _model = player;
            _hitHandler = hitHandler;
        }

        public bool IsDead
        {
            get { return _model.IsDead; }
        }

        public Vector3 Position
        {
            get { return _model.Position; }
        }

        public Quaternion Rotation
        {
            get { return _model.Rotation; }
        }

        public void TakeDamage(Vector3 moveDirection)
        {
            _hitHandler.TakeDamage(moveDirection);
        }

        private void Update()
        {
            GamePause();
        }

        

        public void GamePause()
        {
            if (_model.Health <= 0 && Time.time > 0)
            {
                gameOverWindow.SetActive(true);
                Time.timeScale = 0;

            }
            else
            {
                Time.timeScale = 1;
                gameOverWindow.SetActive(false);
            }
        }
    }
}
